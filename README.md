## Jenkins + Snyk

This demo environment consists of an application that has CI jobs in Jenkins and runs a Snyk scan for security. The results are parsed from Snyk's JSON output to our dependency scanning file output.